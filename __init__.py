from ctx import Context
from st3m.application import Application, ApplicationContext
from st3m.input import InputState, InputController
from st3m.reactor import Responder
from st3m.ui import colours
from st3m import logging
import network
import st3m.run

UPDATE_TIME_MS = 250


class WifiStatus(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

        self.log = logging.Log(__name__, level=logging.INFO)
        self.input = InputController()
        self.wifi = network.WLAN(network.STA_IF)
        self.status = ""
        self.rssi = 0.0
        self.dT_ms = 0

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.save()
        ctx.move_to(0, 0)
        ctx.rgb(*colours.WHITE)
        ctx.font_size = 24
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.BEVEL
        if self.status != "":
            ctx.text(self.status)

            ctx.move_to(0, 40)
            ctx.font_size = 12
            ctx.text("Press App button to connect")
        else:
            ctx.text(f"RSSI: {self.rssi}")
            ctx.rgb(*colours.WHITE).rectangle(-100, 40, 120 + self.rssi, 40).fill()

        ctx.restore()

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)

        if self.input.buttons.app.middle.pressed:
            try:
                self.log.info("connecting to WiFi")
                self.wifi.active(False)
                self.wifi.active(True)
                self.wifi.connect("Camp2023-open")
            except Exception as e:
                self.status = str(e)
                pass

        self.dT_ms += delta_ms
        if self.dT_ms < UPDATE_TIME_MS:
            pass

        self.dT_ms = self.dT_ms % UPDATE_TIME_MS

        try:
            if not self.wifi.active():
                self.status = "WiFi disabled"
                pass

            if not self.wifi.isconnected():
                self.status = "WiFi disconnected"
                pass
            
            self.status = ""
            self.rssi = self.wifi.status("rssi")
        except Exception as e:
            self.status = str(e)
            self.rssi = -120


if __name__ == "__main__":
    st3m.run.run_view(WifiStatus(ApplicationContext()))
